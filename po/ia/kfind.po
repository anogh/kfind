# Copyright (C) YEAR This_file_is_part_of_KDE
# This file is distributed under the same license as the PACKAGE package.
#
# g.sora <g.sora@tiscali.it>, 2010, 2011, 2013, 2016, 2017, 2018, 2020, 2021, 2023.
msgid ""
msgstr ""
"Project-Id-Version: \n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2023-09-19 01:56+0000\n"
"PO-Revision-Date: 2023-03-14 23:27+0100\n"
"Last-Translator: giovanni <g.sora@tiscali.it>\n"
"Language-Team: Interlingua <kde-i18n-doc@kde.org>\n"
"Language: ia\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 22.12.3\n"

#: kfinddlg.cpp:31
#, kde-format
msgctxt "@title:window"
msgid "Find Files/Folders"
msgstr "Trova files/dossieres"

#: kfinddlg.cpp:48 kfinddlg.cpp:197
#, kde-format
msgctxt "the application is currently idle, there is no active search"
msgid "Idle."
msgstr "Inactive."

#. i18n as below
#: kfinddlg.cpp:133
#, kde-format
msgid "0 items found"
msgstr "0 elementos trovate"

#: kfinddlg.cpp:172
#, kde-format
msgid "Searching..."
msgstr "On Initia cerca..."

#: kfinddlg.cpp:199
#, kde-format
msgid "Canceled."
msgstr "Cancellate."

#: kfinddlg.cpp:201 kfinddlg.cpp:204 kfinddlg.cpp:207
#, kde-format
msgid "Error."
msgstr "Error."

#: kfinddlg.cpp:202
#, kde-format
msgid "Please specify an absolute path in the \"Look in\" box."
msgstr ""
"Pro favor tu specifica un percurso absolute in le quadrato de \"Guarda intra"
"\"."

#: kfinddlg.cpp:205
#, kde-format
msgid "Could not find the specified folder."
msgstr "Il non poteva trovar le specificate dossier."

#: kfinddlg.cpp:228 kfinddlg.cpp:252
#, kde-format
msgid "one item found"
msgid_plural "%1 items found"
msgstr[0] "un elemento trovate"
msgstr[1] "%1 elementos trovate"

#: kfindtreeview.cpp:45
msgid "Read-write"
msgstr "Lectura-Scriptura"

#: kfindtreeview.cpp:46
msgid "Read-only"
msgstr "Solmente de lectura"

#: kfindtreeview.cpp:47
msgid "Write-only"
msgstr "Solmente de scriptura"

#: kfindtreeview.cpp:48
msgid "Inaccessible"
msgstr "Non accessibile"

#: kfindtreeview.cpp:68
#, kde-format
msgctxt "file name column"
msgid "Name"
msgstr "Nomine"

#: kfindtreeview.cpp:70
#, kde-format
msgctxt "name of the containing folder"
msgid "In Subfolder"
msgstr "In sub dossier"

#: kfindtreeview.cpp:72
#, kde-format
msgctxt "file size column"
msgid "Size"
msgstr "Grandor"

#: kfindtreeview.cpp:74
#, kde-format
msgctxt "modified date column"
msgid "Modified"
msgstr "Modificate"

#: kfindtreeview.cpp:76
#, kde-format
msgctxt "file permissions column"
msgid "Permissions"
msgstr "Permissiones"

#: kfindtreeview.cpp:78
#, kde-format
msgctxt "first matching line of the query string in this file"
msgid "First Matching Line"
msgstr "Prime linea coincidente"

#: kfindtreeview.cpp:361
#, kde-format
msgctxt "@action:incontextmenu"
msgid "Copy Location"
msgstr "Copia location"

#: kfindtreeview.cpp:362
#, kde-format
msgctxt "@info:whatsthis copy_location"
msgid "This will copy the path of the first selected item into the clipboard."
msgstr ""
"Isto copiara le percurso del prime elemento seligite in le area de "
"transferentia."

#: kfindtreeview.cpp:367
#, kde-format
msgid "&Open containing folder(s)"
msgstr "&Aperi dossier(es) que contine"

#: kfindtreeview.cpp:371
#, kde-format
msgid "&Delete"
msgstr "&Dele"

#: kfindtreeview.cpp:375
#, kde-format
msgid "&Move to Trash"
msgstr "&Move al corbe"

#: kfindtreeview.cpp:515
#, kde-format
msgctxt "@title:window"
msgid "Save Results As"
msgstr "Salveguarda resultatos como"

#: kfindtreeview.cpp:540
#, kde-format
msgid "Unable to save results."
msgstr "Inhabile a salveguardar resultatos."

#: kfindtreeview.cpp:554
#, kde-format
msgid "KFind Results File"
msgstr "File de resultatos de KFind"

#: kfindtreeview.cpp:569
#, kde-format
msgctxt "%1=filename"
msgid "Results were saved to: %1"
msgstr "Resultatos esseva salveguardate a : %1"

#: kfindtreeview.cpp:665 kftabdlg.cpp:396
#, kde-format
msgid "&Properties"
msgstr "&Proprietates"

#: kftabdlg.cpp:66
#, kde-format
msgctxt "this is the label for the name textfield"
msgid "&Named:"
msgstr "&Nominate:"

#: kftabdlg.cpp:69
#, kde-format
msgid "You can use wildcard matching and \";\" for separating multiple names"
msgstr "Tu pote usar wildcard coincidente e \";\" pro separar nomine multiple"

#: kftabdlg.cpp:75
#, kde-format
msgid "Look &in:"
msgstr "Guarda &intra:"

#: kftabdlg.cpp:78
#, kde-format
msgid "Include &subfolders"
msgstr "Include &sub dossieres"

#: kftabdlg.cpp:79
#, kde-format
msgid "Case s&ensitive search"
msgstr "Cerca s&ensibile al caso (differentias inter majusculas e minusculas)"

#: kftabdlg.cpp:80
#, kde-format
msgid "&Browse..."
msgstr "&Naviga..."

#: kftabdlg.cpp:81
#, kde-format
msgid "&Use files index"
msgstr "&Usa indice de files"

#: kftabdlg.cpp:82
#, kde-format
msgid "Show &hidden files"
msgstr "&Monstra files celate"

#: kftabdlg.cpp:101
#, kde-format
msgid ""
"<qt>Enter the filename you are looking for. <br />Alternatives may be "
"separated by a semicolon \";\".<br /><br />The filename may contain the "
"following special characters:<ul><li><b>?</b> matches any single character</"
"li><li><b>*</b> matches zero or more of any characters</li><li><b>[...]</b> "
"matches any of the characters between the braces</li></ul><br />Example "
"searches:<ul><li><b>*.kwd;*.txt</b> finds all files ending with .kwd or ."
"txt</li><li><b>go[dt]</b> finds god and got</li><li><b>Hel?o</b> finds all "
"files that start with \"Hel\" and end with \"o\", having one character in "
"between</li><li><b>My Document.kwd</b> finds a file of exactly that name</"
"li></ul></qt>"
msgstr ""
"<qt>Tu inserta le nomine de file que tu es cercante. <br />Alternativas pote "
"esser separate per un puncto e virgula \";\".<br /><br />Le nomine de file "
"pote continer le sequente characteres special:<ul><li><b>?</b> il face "
"corresponder omne singule character</li><li><b>*</b> il face corresponder "
"zero o plus de altere characteres</li><li><b>[...]</b> il face corresponder "
"omne characteres inter le parentheses</li></ul><br />Exemplo il cerca:"
"<ul><li><b>*.kwd;*.txt</b> il trova omne files terminante con .kwd o .txt</"
"li><li><b>go[dt]</b> il trova god e got</li><li><b>Hel?o</b> il trova omne "
"files que initia con \"Hel\" e termina con \"o\", habente un character intra "
"</li><li><b>My Document.kwd</b> il trova un file con exactemente ille "
"nomine</li></ul></qt>"

#: kftabdlg.cpp:122
#, kde-format
msgid ""
"<qt>This lets you use the files' index created by the <i>slocate</i> package "
"to speed-up the search; remember to update the index from time to time "
"(using <i>updatedb</i>).</qt>"
msgstr ""
"<qt>Isto permitte te de usar le indice de create per le pacchetto "
"<i>slocate</i> package pro adder velocitate al cerca; tu memora de "
"actualisar le index aliquando (usante <i>updatedb</i>).</qt>"

#: kftabdlg.cpp:166
#, kde-format
msgid "Find all files created or &modified:"
msgstr "Trova omne files create o &modificate:"

#: kftabdlg.cpp:168
#, kde-format
msgid "&between"
msgstr "&inter"

#: kftabdlg.cpp:170
#, kde-format
msgid "and"
msgstr "e"

#: kftabdlg.cpp:192
#, kde-format
msgid "File &size is:"
msgstr "Dimen&sion de file es:"

#: kftabdlg.cpp:205
#, kde-format
msgid "Files owned by &user:"
msgstr "Files possedite per &usator"

#: kftabdlg.cpp:210
#, kde-format
msgid "Owned by &group:"
msgstr "Possedite per &gruppo:"

#: kftabdlg.cpp:213
#, kde-format
msgctxt "file size isn't considered in the search"
msgid "(none)"
msgstr "(necun)"

#: kftabdlg.cpp:214
#, kde-format
msgid "At Least"
msgstr "Al minus"

#: kftabdlg.cpp:215
#, kde-format
msgid "At Most"
msgstr "Al plus"

#: kftabdlg.cpp:216
#, kde-format
msgid "Equal To"
msgstr "Equal a"

#: kftabdlg.cpp:218 kftabdlg.cpp:829
#, kde-format
msgid "Byte"
msgid_plural "Bytes"
msgstr[0] "Byte"
msgstr[1] "Bytes"

#: kftabdlg.cpp:219
#, kde-format
msgid "KiB"
msgstr "KiB"

#: kftabdlg.cpp:220
#, kde-format
msgid "MiB"
msgstr "MiB"

#: kftabdlg.cpp:221
#, kde-format
msgid "GiB"
msgstr "GiB"

#: kftabdlg.cpp:284
#, kde-format
msgctxt "label for the file type combobox"
msgid "File &type:"
msgstr "&typo de file:"

#: kftabdlg.cpp:289
#, kde-format
msgid "C&ontaining text:"
msgstr "Texto c&ontinente:"

#: kftabdlg.cpp:295
#, kde-format
msgid ""
"<qt>If specified, only files that contain this text are found. Note that not "
"all file types from the list above are supported. Please refer to the "
"documentation for a list of supported file types.</qt>"
msgstr ""
"<qt>Si specificate, solmente files que contine isto text es trovate. Tu nota "
"que non omne typos de file ex le lista de supra es supportate. Pro favor, tu "
"refere al documentation per un lista de typos de file supportate file types."
"</qt>"

#: kftabdlg.cpp:303
#, kde-format
msgid "Case s&ensitive"
msgstr "Distingu&e inter majusculas e minusculas"

#: kftabdlg.cpp:304
#, kde-format
msgid "Include &binary files"
msgstr "Il include files &binari"

#: kftabdlg.cpp:307
#, kde-format
msgid ""
"<qt>This lets you search in any type of file, even those that usually do not "
"contain text (for example program files and images).</qt>"
msgstr ""
"<qt>Isto permitte te de cercar in omne typo de file, etiam illos que "
"usualmente non contine texto (pro exemplo files de programma e images).</qt>"

#: kftabdlg.cpp:314
#, kde-format
msgctxt "as in search for"
msgid "fo&r:"
msgstr "p&ro:"

#: kftabdlg.cpp:316
#, kde-format
msgid "Search &metainfo sections:"
msgstr "Cerca sectiones de &metainfo"

#: kftabdlg.cpp:320
#, kde-format
msgid "All Files & Folders"
msgstr "Omne files & dossieres"

#: kftabdlg.cpp:321
#, kde-format
msgid "Files"
msgstr "Files"

#: kftabdlg.cpp:322
#, kde-format
msgid "Folders"
msgstr "Dossieres"

#: kftabdlg.cpp:323
#, kde-format
msgid "Symbolic Links"
msgstr "Ligamines symbolic"

#: kftabdlg.cpp:324
#, kde-format
msgid "Special Files (Sockets, Device Files, ...)"
msgstr "Files special (Sockets, Files de dispositivo, ...)"

#: kftabdlg.cpp:325
#, kde-format
msgid "Executable Files"
msgstr "Files que on pote executar"

#: kftabdlg.cpp:326
#, kde-format
msgid "SUID Executable Files"
msgstr "Files que SUID pote executar"

#: kftabdlg.cpp:327
#, kde-format
msgid "All Images"
msgstr "Omne Images"

#: kftabdlg.cpp:328
#, kde-format
msgid "All Video"
msgstr "Omne Video"

#: kftabdlg.cpp:329
#, kde-format
msgid "All Sounds"
msgstr "Omne Sonos"

#: kftabdlg.cpp:394
#, kde-format
msgid "Name/&Location"
msgstr "Nomine/&Location"

#: kftabdlg.cpp:395
#, kde-format
msgctxt "tab name: search by contents"
msgid "C&ontents"
msgstr "C&ontentos"

#: kftabdlg.cpp:400
#, kde-format
msgid ""
"<qt>Search within files' specific comments/metainfo<br />These are some "
"examples:<br /><ul><li><b>Audio files (mp3...)</b> Search in id3 tag for a "
"title, an album</li><li><b>Images (png...)</b> Search images with a special "
"resolution, comment...</li></ul></qt>"
msgstr ""
"<qt>Cerca intra specific commentos/metainfo<br /> de file. Istos es alcun "
"exemplos:<br /><ul><li><b>Files Audio (mp3...)</b> Cerca etiquetta id3 pro "
"un titulo, un album</li><li><b>Images (png...)</b> Cerca images con un "
"special resolution, commento...</li></ul></qt>"

#: kftabdlg.cpp:408
#, kde-format
msgid ""
"<qt>If specified, search only in this field<br /><ul><li><b>Audio files "
"(mp3...)</b> This can be Title, Album...</li><li><b>Images (png...)</b> "
"Search only in Resolution, Bitdepth...</li></ul></qt>"
msgstr ""
"<qt>Si specificate, il cerca solmente in iste campo<br /><ul><li><b>Files "
"Audio (mp3...)</b> Isto pote esser Titulo, Album...</li><li><b>Images "
"(png...)</b> Cerca solmente in Resolution, Bitdepth...</li></ul></qt>"

#: kftabdlg.cpp:549
#, kde-format
msgid "Unable to search within a period which is less than a minute."
msgstr ""
"Il es inhabile a cercar intra un periodo que il es minor que un minuta."

#: kftabdlg.cpp:560
#, kde-format
msgid "The date is not valid."
msgstr "Le data non es valide"

#: kftabdlg.cpp:562
#, kde-format
msgid "Invalid date range."
msgstr "Invalide scala de data."

#: kftabdlg.cpp:564
#, kde-format
msgid "Unable to search dates in the future."
msgstr "On non pote cercar datas in le futuro."

#: kftabdlg.cpp:636
#, kde-format
msgid "Size is too big. Set maximum size value?"
msgstr "Dimension es troppo grande. Tu fixa le maximum valor de dimension?"

#: kftabdlg.cpp:636
#, kde-format
msgid "Error"
msgstr "Error"

#: kftabdlg.cpp:636
#, kde-format
msgid "Set"
msgstr "Fixa"

#: kftabdlg.cpp:636
#, kde-format
msgid "Do Not Set"
msgstr "Non Fixa"

#: kftabdlg.cpp:819
#, kde-format
msgctxt ""
"during the previous minute(s)/hour(s)/...; dynamic context 'type': 'i' "
"minutes, 'h' hours, 'd' days, 'm' months, 'y' years"
msgid "&during the previous"
msgid_plural "&during the previous"
msgstr[0] "&durante le precedente"
msgstr[1] "&durante le precedente"

#: kftabdlg.cpp:820
#, kde-format
msgctxt "use date ranges to search files by modified time"
msgid "minute"
msgid_plural "minutes"
msgstr[0] "minuta"
msgstr[1] "minutas"

#: kftabdlg.cpp:821
#, kde-format
msgctxt "use date ranges to search files by modified time"
msgid "hour"
msgid_plural "hours"
msgstr[0] "hora"
msgstr[1] "horas"

#: kftabdlg.cpp:822
#, kde-format
msgctxt "use date ranges to search files by modified time"
msgid "day"
msgid_plural "days"
msgstr[0] "die"
msgstr[1] "dies"

#: kftabdlg.cpp:823
#, kde-format
msgctxt "use date ranges to search files by modified time"
msgid "month"
msgid_plural "months"
msgstr[0] "mense"
msgstr[1] "menses"

#: kftabdlg.cpp:824
#, kde-format
msgctxt "use date ranges to search files by modified time"
msgid "year"
msgid_plural "years"
msgstr[0] "anno"
msgstr[1] "annos"

#: kquery.cpp:556
#, kde-format
msgctxt "@title:window"
msgid "Error while using locate"
msgstr "Error durante que il usava locate"

#: main.cpp:26
#, kde-format
msgid "KFind"
msgstr "KFind"

#: main.cpp:27
#, kde-format
msgid "KDE file find utility"
msgstr "Utilitate de trovar file de KDE"

#: main.cpp:28
#, kde-format
msgid "(c) 1998-2021, The KDE Developers"
msgstr "(c) 1998-2021, Le disveloppatores de KDE"

#: main.cpp:30
#, kde-format
msgid "Kai Uwe Broulik"
msgstr "Kai Uwe Broulik"

#: main.cpp:30
#, kde-format
msgid "Current Maintainer"
msgstr "Mantenitor Currente"

#: main.cpp:31
#, kde-format
msgid "Eric Coquelle"
msgstr "Eric Coquelle"

#: main.cpp:31
#, kde-format
msgid "Former Maintainer"
msgstr "Mantenitor Precedente"

#: main.cpp:32
#, kde-format
msgid "Mark W. Webb"
msgstr "Mark W. Webb"

#: main.cpp:32
#, kde-format
msgid "Developer"
msgstr "Developpator"

#: main.cpp:33
#, kde-format
msgid "Beppe Grimaldi"
msgstr "Beppe Grimaldi"

#: main.cpp:33
#, kde-format
msgid "UI Design & more search options"
msgstr "Designo de UI & altere optiones de cerca"

#: main.cpp:34
#, kde-format
msgid "Martin Hartig"
msgstr "Martin Hartig"

#: main.cpp:35
#, kde-format
msgid "Stephan Kulow"
msgstr "Stephan Kulow"

#: main.cpp:36
#, kde-format
msgid "Mario Weilguni"
msgstr "Mario Weilguni"

#: main.cpp:37
#, kde-format
msgid "Alex Zepeda"
msgstr "Alex Zepeda"

#: main.cpp:38
#, kde-format
msgid "Miroslav Flídr"
msgstr "Miroslav Flídr"

#: main.cpp:39
#, kde-format
msgid "Harri Porten"
msgstr "Harri Porten"

#: main.cpp:40
#, kde-format
msgid "Dima Rogozin"
msgstr "Dima Rogozin"

#: main.cpp:41
#, kde-format
msgid "Carsten Pfeiffer"
msgstr "Carsten Pfeiffer"

#: main.cpp:42
#, kde-format
msgid "Hans Petter Bieker"
msgstr "Hans Petter Bieker"

#: main.cpp:43
#, kde-format
msgid "Waldo Bastian"
msgstr "Waldo Bastian"

#: main.cpp:43
#, kde-format
msgid "UI Design"
msgstr "Designo de UI"

#: main.cpp:44
#, kde-format
msgid "Alexander Neundorf"
msgstr "Alexander Neundorf"

#: main.cpp:45
#, kde-format
msgid "Clarence Dang"
msgstr "Clarence Dang"

#: main.cpp:46
#, kde-format
msgctxt "NAME OF TRANSLATORS"
msgid "Your names"
msgstr "Giovanni Sora"

#: main.cpp:46
#, kde-format
msgctxt "EMAIL OF TRANSLATORS"
msgid "Your emails"
msgstr "g.sora@tiscali.it"

#: main.cpp:49
#, kde-format
msgid "Path(s) to search"
msgstr "Percurso(s) de cercar"

#~ msgid "Regular e&xpression"
#~ msgstr "E&xpression regular"

#~ msgid "&Edit..."
#~ msgstr "&Edita..."

#~ msgid "HTML page"
#~ msgstr "pagina HTML"

#~ msgid "Text file"
#~ msgstr "file de Texto"

#~ msgid "Copyright © 1998–2011 KFind authors"
#~ msgstr "Copyright © 1998–2011 autores de KFind"
